# material-decomposition
Analysis script for the article Material Decomposition in Low-Energy Micro-CT Using a Dual-Threshold Photon Counting X-Ray Detector (DOI: https://doi.org/10.3389/fphy.2021.673843).

Correspondig data available at: https://osf.io/wmvn2/

