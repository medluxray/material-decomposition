# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 19:59:10 2021
@author: Rasmus Solem


Script for post image reconstruction material decomposition described in:
Solem, R., Dreier, T. and Bech, M. (2021).
Material decomposition in low-energy micro-CT using a dual-threshold photon counting X-ray detector. 
 
Data avilable at osf.io/wmvn2


"""

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from scipy.optimize import nnls
import PIL
from matplotlib_scalebar.scalebar import ScaleBar
import seaborn as sns

sns.set_color_codes()
plt.style.use('seaborn')
plt.set_cmap('viridis')


# %% PATHS and SETTINGS

# Select path and folders for decomposition data
path = r"F:\xraylab\Eiger\Material_Decomposition_Data"
lower_folder = "Small_Plaque_lower"
upper_folder = "Small_Plaque_upper"

# Window energies (keV) that the decomposition is based on
lower_window = '5.5-7' 
upper_window = '7.5-9.5'
Materials = ['Iron', 'Calcium', 'Paraffin']

eff_pixel = 13.63636 # effective pixel size in micrometers
recon_slice = 180 # slice for decomposition

# read in tiff images into lower and upper stacks
decomp_lower_stack = np.array([np.asarray(PIL.Image.open(os.path.join(path, lower_folder, dfile_lower))) for dfile_lower in os.listdir(os.path.join(path, lower_folder))])
decomp_upper_stack = np.array([np.asarray(PIL.Image.open(os.path.join(path, upper_folder, dfile_upper))) for dfile_upper in os.listdir(os.path.join(path, upper_folder))])

# %% Preview one slice

plt.imshow(decomp_upper_stack[recon_slice])
clb = plt.colorbar()
clb.ax.set_ylabel('$\mu$ [-]')
scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0, color = 'w', location='lower left') 
plt.gca().add_artist(scalebar)
plt.grid(False)
plt.title('Upper energy window slice preview')
plt.show()

# %% SETUP ROI -> crop images (for both energy windows)
# x = horizontal, y = vertical
roi_x_start =270 
roi_x_stop = 460 

roi_y_start = 370 
roi_y_stop = 460 

# crop all images, divide by effective pixel size to get correct attenuation value unit
decomp_l = (decomp_lower_stack[recon_slice][roi_y_start:roi_y_stop, roi_x_start:roi_x_stop])/eff_pixel
decomp_u = (decomp_upper_stack[recon_slice][roi_y_start:roi_y_stop, roi_x_start:roi_x_stop])/eff_pixel 

im = plt.imshow(decomp_u)
clb = plt.colorbar()
clb.ax.set_ylabel('$\mu$ [$\mu$m$^{-1}$]')#
scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0, color = 'w', location='lower right') 
plt.gca().add_artist(scalebar)
plt.axis('off')
plt.grid(False)
plt.title('Croped upper energy window preview')
plt.show()


# %% Region selection for attenutaion decomposition values for Fe, Ca and Paraffin 

# x = horizontal, y = vertical
Fe_x = 65
Fe_y = 73
Fe_size = 2

Ca_x = 123
Ca_y = 28
Ca_size = 2

P_x = 35
P_y = 10
P_size = 10

# Plot ROIs in upper energy window
fig, ax = plt.subplots(1)
im = ax.imshow(decomp_u) 
rect_Fe = Rectangle((Fe_x, Fe_y), Fe_size, Fe_size, linewidth=2, edgecolor='r', facecolor='none', label="Fe")
ax.add_patch(rect_Fe)
rect_Ca = Rectangle((Ca_x, Ca_y), Ca_size, Ca_size, linewidth=2, edgecolor='green', facecolor='none', label="Ca")
ax.add_patch(rect_Ca)
rect_p = Rectangle((P_x, P_y), P_size, P_size, linewidth=2, edgecolor='b', facecolor='none', label="Paraffin")
ax.add_patch(rect_p)
plt.legend(loc='lower left', labelcolor='w')
ax.set_title('ROIs for material decomposition attenuation values')
clb = plt.colorbar(im)
clb.ax.set_ylabel('$\mu$ [$\mu$m$^{-1}$]')#
scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0, color='w',  location='lower right')
plt.gca().add_artist(scalebar)
plt.axis('off')
plt.grid(False)
plt.show()

# Adding material values as mean of each ROI to array, one row per energy window
Fe_material_values = np.array([[np.mean(decomp_l[Fe_y:Fe_y+Fe_size, Fe_x:Fe_x+Fe_size])], [np.mean(decomp_u[Fe_y:Fe_y+Fe_size, Fe_x:Fe_x+Fe_size])]])
Ca_material_values = np.array([[np.mean(decomp_l[Ca_y:Ca_y+Ca_size, Ca_x:Ca_x+Ca_size])], [np.mean(decomp_u[Ca_y:Ca_y+Ca_size, Ca_x:Ca_x+Ca_size])]])
P_material_values = np.array([[np.mean(decomp_l[P_y:P_y+P_size, P_x:P_x+P_size])], [np.mean(decomp_u[P_y:P_y+P_size, P_x:P_x+P_size])]])

Material_Values = np.hstack((Fe_material_values, Ca_material_values, P_material_values))


# %%  Performing the material decomposition

# Creating material decomposition matrix
Material_Values_Matrix = np.vstack((Material_Values, np.ones(len(Materials))))

#Shape images to one row arrays
Lower_as_row = np.matrix.flatten(decomp_l)
Upper_as_row = np.matrix.flatten(decomp_u)

#Create 3 row matrix: lower, upper, ones
Image_Values_Matrix = np.array([Lower_as_row, Upper_as_row, np.ones(decomp_l.size)])

# Calculating the material fractions F one pixel at a time with non-negative constraint 
F = []
for idx in range(len(Lower_as_row)):
    F.append(nnls(Material_Values_Matrix, Image_Values_Matrix[:,idx])[0])
F = np.array(F).T

# Reshape F rows into 3 image matrices
Image_shape = decomp_l.shape
Material_images = [np.reshape(F[mat,:], Image_shape) for mat in range(len(Materials))]


#%% Plot lower and upper energy windows and material decomposition images

plt.imshow(decomp_l)
plt.title('Lower energy window image, {} keV'.format(lower_window))
clb = plt.colorbar()
clb.ax.set_ylabel('$\mu$ [$\mu$m$^{-1}$]')#
scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0, color='w',location='lower right') 
plt.gca().add_artist(scalebar)
plt.axis('off')
plt.grid(False)
plt.show()


plt.imshow(decomp_u)
plt.title('Upper energy window image, {} keV'.format(upper_window))
clb = plt.colorbar()
clb.ax.set_ylabel('$\mu$ [$\mu$m$^{-1}$]')#
scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0,color='w', location='lower right') 
plt.gca().add_artist(scalebar)
plt.axis('off')
plt.grid(False)
plt.show()


colormap=['Reds', 'Greens', 'Blues']
for idx, img in enumerate(Material_images):
    plt.imshow(img, cmap=colormap[idx])
    clb = plt.colorbar()
    clb.ax.set_ylabel('Material fraction')
    scalebar = ScaleBar(dx=eff_pixel, units='um', box_alpha=0, color='w', location='lower right') 
    plt.gca().add_artist(scalebar)
    plt.title('Decomposed {}'.format(Materials[idx]))
    plt.axis('off')
    plt.grid(False)
    plt.show()


